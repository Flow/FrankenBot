name := "FrankenBot"

version := "0.1"

mainClass in Compile := Some("eu.geekplace.frankenbot.core.FrankenBotMain")

scalaVersion := "2.12.3"
val smackVersion = "4.2.1"
val akkaVersion = "2.5.6"

resolvers += Resolver.sonatypeRepo("snapshots")
resolvers += Resolver.mavenLocal

libraryDependencies += "com.typesafe.scala-logging" %% "scala-logging" % "3.7.2"
libraryDependencies += "org.slf4j" % "jul-to-slf4j" % "1.7.25"
libraryDependencies += "ch.qos.logback" % "logback-classic" % "1.2.3"

libraryDependencies += "org.igniterealtime.smack" % "smack-tcp" % smackVersion
libraryDependencies += "org.igniterealtime.smack" % "smack-java7" % smackVersion
libraryDependencies += "org.igniterealtime.smack" % "smack-extensions" % smackVersion
libraryDependencies += "org.igniterealtime.smack" % "smack-experimental" % smackVersion

libraryDependencies += "org.rrd4j" % "rrd4j" % "3.1"

libraryDependencies += "com.typesafe.akka" %% "akka-actor" % akkaVersion
libraryDependencies += "com.typesafe.akka" %% "akka-slf4j" % akkaVersion

libraryDependencies += "org.pircbotx" % "pircbotx" % "2.0.1"

libraryDependencies += "net.jcazevedo" %% "moultingyaml" % "0.4.0"

libraryDependencies += "eu.geekplace.javapinning" % "java-pinning-java7" % "1.1.0-alpha1"

libraryDependencies += "com.github.scopt" %% "scopt" % "3.7.0"

libraryDependencies += "org.apache.commons" % "commons-lang3" % "3.6"

lazy val scalatest = "org.scalatest" %% "scalatest" % "3.0.1"
libraryDependencies += scalatest % Test

EclipseKeys.withSource := true
EclipseKeys.withJavadoc := true
