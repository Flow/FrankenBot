/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.core.configuration.yaml

import net.jcazevedo.moultingyaml._
import org.jxmpp.jid._
import org.jxmpp.jid.impl.JidCreate

object JidYamlFormat extends DefaultYamlProtocol {

  implicit object EntityBareJidYamlFormat extends YamlFormat[EntityBareJid] {
    def write(j: EntityBareJid) =
      YamlString(j.toString)

    def read(value: YamlValue) = value match {
      case YamlString(j) => JidCreate entityBareFrom j
      case _ => deserializationError("Entity bare JID String expected")
    }
  }

}