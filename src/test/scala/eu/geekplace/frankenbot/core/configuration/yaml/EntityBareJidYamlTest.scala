/**
 *
 * Copyright 2017 Florian Schmaus
 *
 * This file is part of FrankenBot.
 *
 * FrankenBot is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software Foundation,
 * Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301  USA
 */
package eu.geekplace.frankenbot.core.configuration.yaml

import org.jxmpp.jid.EntityBareJid
import org.scalatest._
import net.jcazevedo.moultingyaml._
import JidYamlFormat._
import org.jxmpp.jid.impl.JidCreate
import scala.collection.mutable.StringBuilder

class EntityBareJidYamlTest extends FunSuite with Matchers {

  test("Entity Bare JID YAML Test") {
    val jids = List(
                 "foo@example.org"
               , "bar@example.org"
               , "baz@example.org"
               )

    val listOfEntityBareJidsYaml = new StringBuilder()
    
    jids foreach {
      listOfEntityBareJidsYaml.append("- ").append(_).append('\n')
    }

    val parsedListOfEntityBareJids = listOfEntityBareJidsYaml.toString.parseYaml

    val listOfEntityBareJids = parsedListOfEntityBareJids.convertTo[List[EntityBareJid]]

    for ((jid, i) <- listOfEntityBareJids.zipWithIndex) {
      val expectedJid = JidCreate entityBareFrom jids(i)
      jid should be (expectedJid)
    }

  }
}